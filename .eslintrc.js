module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb'
  ],
  rules: {
    indent: ['error', 2],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    semi: [2, 'never'],
    'linebreak-style': 0,
    'comma-dangle': ['error', 'never'],
    'arrow-body-style': 0,
    'import/prefer-default-export': 0,
    'global-require': 0,
    'no-unused-vars': 0,
    'arrow-parens': 0,
    'comma-dangle': 0,
    'max-len': ["error", { "code": 200 }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
