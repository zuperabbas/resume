import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/experience',
    name: 'Experience',
    component: () => import('@/views/Experience.vue')
  },
  {
    path: '/education',
    name: 'Education',
    component: () => import('@/views/Education.vue')
  },
  {
    path: '/skills',
    name: 'Skills',
    component: () => import('@/views/Skills.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
