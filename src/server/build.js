const express = require('express')
const path = require('path')
const open = require('open')

process.env.NODE_ENV = 'production'
const app = express()
const DIST_DIR = path.join(__dirname, '../../dist')
const HTML_FILE = path.join(DIST_DIR, 'index.html')
const host = 'localhost'
const port = 3000

app.use(express.static(DIST_DIR))

app.get('*', (req, res) => res.sendFile(HTML_FILE))

app.listen(port, (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log(`> App is listening on port ${process.env.PORT || port}\n`)
  }
})
